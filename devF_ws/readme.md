Uses yaml-cpp library to parse through the yamls. (https://github.com/jbeder/yaml-cpp)
Change the include directory of Cmake as needed (line 47) since I don't try to find it's path.

Is lacking some crucial features if it was to be applied, mainly not having the pose subscriber in parallel which shouldn't be that much of a problem if the yamls weren't required to be parsed each iteration.
Finally, threads are implemented in such basic way where they don't stop searching for the order even if another thread has found it already. Which wastes time.

To source -> ../devF_ws/install/setup.bash

There's a nasty warning regarding using arrays instead of vectors, I idealized only performing that step once instead of recomputing the distances between known parts' positions everytime. I bounced a lot between doing things with scalability in mind and doing things within the time frame, and I think it shows.
