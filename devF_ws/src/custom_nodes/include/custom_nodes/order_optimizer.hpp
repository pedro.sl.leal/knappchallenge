#include <memory>
#include <string> 
#include <thread>

#include <yaml-cpp/yaml.h>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "custom_msgs_srvs/msg/next_order_message.hpp"
#include "visualization_msgs/msg/marker_array.hpp"

class OrderOptimizer : public rclcpp::Node
{
    public: 
        OrderOptimizer();
        
        void currentPosition_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg);
        void nextOrder_callback(const custom_msgs_srvs::msg::NextOrderMessage::SharedPtr msg);
        void findPathandPrint(int current_order, std::string current_description);
        void publishMarkers();
        void threadManager(int order);
        void ThreadTester(std::string file_name, int order, double goal_pos[2], std::vector<int> &goal_products);
        double distance(double a[2], double b[2]);


    private: 
        struct RequiredPart    //A nice struct with the necessary information at the end.
          {
              std::string part_name;
              int part_count;
              double part_position[2];
              int id;
              std::string dep_products;  //products that require it in trying form for final print
          };
          
        rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr current_position_subscriber;
        rclcpp::Subscription<custom_msgs_srvs::msg::NextOrderMessage>::SharedPtr next_order_subscriber;
        rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr marker_publisher;
        std::string file_directory;   //Parameter that should point to "applicants_amr_example_1" file (including it)
    
        
        YAML::Node configuration_data;   //Holds the YAML parsed info of the products
    
    
        std::string data_names[5];                //Yamls names
        double current_pose[2];                   //robot current pose            
        double current_goal_position[2];          //robot current goal
        std::vector<int> current_products;        //current products needed
        std::vector<RequiredPart> required_parts; //current parts need info
        
};