from launch import LaunchDescription
from launch_ros.actions import Node
import os
from ament_index_python.packages import get_package_share_directory
import pathlib
import yaml


def generate_launch_description():
    return LaunchDescription([

        Node(
            package = "custom_nodes",
            executable = "order_optimizer_node",
            name =  "order_optimizer",
            output ="screen",
             parameters=[
                {"path_to_files": "/home/pedro/devF_ws/src/custom_nodes/applicants_amr_example_1/",
                }
            ]
        ),

    ])
