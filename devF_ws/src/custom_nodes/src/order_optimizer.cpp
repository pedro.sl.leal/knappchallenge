#include "custom_nodes/order_optimizer.hpp"

using std::placeholders::_1;

  OrderOptimizer::OrderOptimizer()
  : Node("OrderOptimizer_Node")
  {
    RCLCPP_INFO(this->get_logger(), "Node is On." );

    
    //Parameters
    this->declare_parameter<std::string>("path_to_files", "/home/pedro/devF_ws/src/custom_nodes/applicants_amr_example_1/");
    this->get_parameter("path_to_files", file_directory);
    RCLCPP_INFO(this->get_logger(), "Using directory: '%s'", file_directory.c_str());

    //These files and their names are static so I can just read them as an initalization process. 
    configuration_data = YAML::LoadFile(file_directory + "configuration/products.yaml");
    data_names[0] = file_directory + "orders/orders_20201201.yaml";   //Before you ask, it's past eleven. Let's assume it's always these 5 files.
    data_names[1] = file_directory + "orders/orders_20201202.yaml";
    data_names[2] = file_directory + "orders/orders_20201203.yaml";
    data_names[3] = file_directory + "orders/orders_20201204.yaml";
    data_names[4] = file_directory + "orders/orders_20201205.yaml";

    //Subscribers
    current_position_subscriber = this->create_subscription<geometry_msgs::msg::PoseStamped>("currentPosition", 10, std::bind(&OrderOptimizer::currentPosition_callback, this, _1));  //COME BACK HERE TO CHECK ARGUMENTS
    next_order_subscriber = this->create_subscription<custom_msgs_srvs::msg::NextOrderMessage>("nextOrder", 10, std::bind(&OrderOptimizer::nextOrder_callback, this, _1));  //COME BACK HERE TO CHECK ARGUMENTS
    
    //Publisher
    marker_publisher = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);
    
  }

  void OrderOptimizer::currentPosition_callback(const geometry_msgs::msg::PoseStamped::SharedPtr msg)             //Given that the robot likely doesn't move too fast, not using different threads
                                                                                                  //for this subscription shouldn't make much difference on the calculation of the shortest path
                                                                                                  //Later correction: on a i5 9th gen parsing through the yamls takes significant time and the 
                                                                                                  //pose might be significantly outdated 
  {
    
    current_pose[0] = msg->pose.position.x;
    current_pose[1] = msg->pose.position.y;

    publishMarkers();
   }

  void OrderOptimizer::nextOrder_callback(const custom_msgs_srvs::msg::NextOrderMessage::SharedPtr msg)
  {
    int order;
    std::string description;
    order = msg->order_id;
    description = msg->description;
    this->threadManager(order);  
    
    if (current_products.size()==0){
        RCLCPP_INFO(this->get_logger(), "Order not found");
      return;
    }

    RCLCPP_INFO(this->get_logger(), "Goal pose: x:'%f' y:'%f'", current_goal_position[0], current_goal_position[1] );

    std::string delimiter = " ";
    std::string temp_str, actual_product_number;

    //This should be a function for readability
    std::string it_part;       
    bool part_already_order = false;
    for (std::size_t j=0;j<current_products.size();j++){                    //Iterating the through products  
        int index_product_in_file = current_products[j] - 1;                //Assumes products are related to their index by the number. Otherwise either loop through products or std::find.
        
        for (std::size_t k=0;k<configuration_data[index_product_in_file]["parts"].size();k++){  
          
          it_part = configuration_data[index_product_in_file]["parts"][k]["part"].as<std::string>();   
          //RCLCPP_INFO(this->get_logger(), "Parts needed '%s'", it_part.c_str());     

          std::string temp_str;                 

          for (std::size_t i=0;i<required_parts.size();i++){              //iterate through my current list of parts to be order
              
              if (!required_parts[i].part_name.compare(it_part)){         //if this part has already been ordered, I just add to its count.
                    //RCLCPP_INFO(this->get_logger(), "it part '%s', list part '%s'", it_part.c_str(), required_parts[i].part_name.c_str());
                    required_parts[i].part_count++;
                    temp_str = std::to_string(current_products[j]);     //Treating strings so the final prints looks good;
                    if (!(required_parts[i].dep_products.find(temp_str) != std::string::npos)) {
                      required_parts[i].dep_products = required_parts[i].dep_products + " " + temp_str;
                    }
                    
                    part_already_order=true;
                    break;
              }
          }

          if (!part_already_order){                                        //the part hasn't been order and needs to be added to the vector.
            double cx = configuration_data[index_product_in_file]["parts"][k]["cx"].as<double>();
            double cy = configuration_data[index_product_in_file]["parts"][k]["cy"].as<double>();
            temp_str = std::to_string(current_products[j]);
            required_parts.push_back(RequiredPart{it_part, 1, {cx , cy}, required_parts.size(), temp_str.c_str()});
          }
          part_already_order=false;
        }
    }

    //At this point we have the a vector of the struct "RequiredPart", named required_parts. It contains everything we need to know after receiving orders and checking files.

    publishMarkers();    //Simply publishes stuff using the vector required_parts and the robot pose.

    findPathandPrint(order, description);  //Finds the best path and prints the final message. Would probably help readibility to separate these two processes.
      

    required_parts.clear();
    current_products.clear();   //Don't forget to clear the vector of products
  }




  void OrderOptimizer::findPathandPrint(int current_order, std::string current_description){   //There's probably some libraries of a variant of TSP (with a different ending than start) that do this more effectively, 
                      //but just for the sake of exercising:                                  
  //This should be a universal algorithm that gets the optimal solution. Only cares about the current needed parts and goal/robot pose.

    double part_distances[required_parts.size()][required_parts.size()];                     //Need to convert this to vector. It's late, do later and check for consequences. <--- WARNING
    double starting_distance[required_parts.size()], ending_distance[required_parts.size()]; //Later note: This should actually stay a array if the number of available part types
                                                                                             // is static and it would be required to populate these matrixes once. 
                                                                                             //distance between robot pose and each part, distance between order goal and each part.

    for (std::size_t k=0;k<required_parts.size(); k++){                                      //This could be done only once with all the parts

      starting_distance[k] = distance(current_pose, required_parts[k].part_position);
      ending_distance[k] = distance(current_goal_position, required_parts[k].part_position);

      for (std::size_t j=k+1;j<required_parts.size(); j++){
          part_distances[k][j] = distance(required_parts[k].part_position, required_parts[j].part_position);
          part_distances[j][k] = part_distances[k][j];                                           
      }
    }

    double lowest_cost = std::numeric_limits<double>::infinity();   
    double current_cost = 0;
    std::vector<RequiredPart> current_best_permutation;
    do{

        current_cost += starting_distance[required_parts[0].id];  

        for (std::size_t j=0;j<(required_parts.size()-1); j++){                           // Iterate through the current order and use the previous matrix with calculated costs
                                                                                          // to obtain and accumulate the distance between the current and the next part each iteration.
          current_cost += part_distances[required_parts[j].id][required_parts[j+1].id];
          
        }
                                //At the end add the distance between robot pose and first part, and goal pose and last part.
        current_cost += ending_distance[required_parts[required_parts.size()-1].id];

        if (lowest_cost > current_cost){
          lowest_cost = current_cost;
          current_best_permutation = required_parts;
        }

      current_cost = 0;
      } while (std::next_permutation(required_parts.begin(), required_parts.end(),      //We will permutate the different orders one can grab parts.
                              [](const auto & lhs, const auto & rhs)  
                                { return lhs.id < rhs.id; })); //Needed as < operator doesn't work on a struct, uses id to permute.


    RCLCPP_INFO(this->get_logger(), "Performing order '%i' with description '%s'", current_order, current_description.c_str());
    for (std::size_t k=0;k<required_parts.size();k++){          //Final print. Should probably declare current_best_permutation at the callback and print it there instead of here for readability.

      RCLCPP_INFO(this->get_logger(), "Fetching '%i' of '%s' for products '%s' at x: '%f', y: '%f'", current_best_permutation[k].part_count ,current_best_permutation[k].part_name.c_str(), current_best_permutation[k].dep_products.c_str(), 
                    current_best_permutation[k].part_position[0], current_best_permutation[k].part_position[1]);
    }
    RCLCPP_INFO(this->get_logger(), "Delivering to destination x: '%f', y: '%f'", current_goal_position[0], current_goal_position[1]);

  }


  void OrderOptimizer::publishMarkers(){                                               //Will publish markers according to class' current_pose and the current required_parts 
      auto robot_marker = visualization_msgs::msg::Marker();
      auto marker_array = visualization_msgs::msg::MarkerArray();          
      robot_marker.action = 3;                                         //Clear previous //Change this later so it doesn't clear parts
      marker_array.markers.push_back(robot_marker); 
      marker_publisher->publish(marker_array);
      marker_array.markers.clear();

      robot_marker.header.frame_id = "map";                            //Mark robot
      robot_marker.header.stamp = this->get_clock()->now();
      robot_marker.id = 0;
      robot_marker.type = 1;
      robot_marker.action = 0;
      robot_marker.pose.position.x = current_pose[0];
      robot_marker.pose.position.y = current_pose[1];
      robot_marker.color.r = 0.5;
      robot_marker.color.a = 1.;
      robot_marker.lifetime.sec = 5;
      robot_marker.scale.x = 1.0;
      robot_marker.scale.y = 1.0;
      robot_marker.scale.z = 2.0; //"CUBE"
      
      marker_array.markers.push_back(robot_marker); 

      if (required_parts.size()==0){                                    //No orders were received but lets still send robot's marker
        marker_publisher->publish(marker_array);
      }
      else {                                                            //Mark parts
        robot_marker.type = 3;
        for (std::size_t k=0;k<required_parts.size();k++){  
          robot_marker.pose.position.x = required_parts[k].part_position[0];
          robot_marker.pose.position.y = required_parts[k].part_position[1];
          robot_marker.lifetime.sec = 0;
          robot_marker.id = k+1;
          marker_array.markers.push_back(robot_marker); 
        }
        marker_publisher->publish(marker_array);
      }

      marker_array.markers.clear();
  }

  void OrderOptimizer::threadManager(int order) {           //This could be done better, when a thread finds the wanted order others should shut down.
    
    std::vector<std::thread> threads;  //No need to use vectors since it's a static number of YAMLs in this particular case
    const int number_of_threads = 5;   //5 YAMLs of orders //Shouldn't be here and instead be associated with the number of YAMLs. If one wanted to make it more universal it could be
                                        //data_names.size() where data_names size would be a vector instead

    for (uint64_t i = 0; i < number_of_threads; i++) {  //Populates de vector with 5 threads while assigning the function to them as while as arguments
      threads.push_back(std::thread(&OrderOptimizer::ThreadTester, this, data_names[i], order, std::ref(current_goal_position), std::ref(current_products)));
    }

    for (std::thread &t : threads) {      
      if (t.joinable()) {
        t.join();
      }
    }
  }

  void OrderOptimizer::ThreadTester(std::string file_name, int order, double goal_pos[2], std::vector<int> &goal_products) {  //Doesn't account for cases where two threads detect the same order.
                                                                                                              //No need for lock since orders are exclusive to each file

    YAML::Node data;
    data = YAML::LoadFile(file_name);
   
    for (std::size_t i=0;i<data.size();i++) {
      if (data[i]["order"].as<int>() == order){
            
        goal_pos[0] = data[i]["cx"].as<double>();
        goal_pos[1] = data[i]["cy"].as<double>();
      
        for (std::size_t j = 0; j < data[i]["products"].size(); j++) {             
              goal_products.push_back(data[i]["products"][j].as<int>());
              //RCLCPP_INFO(this->get_logger(), "Index: '%i' ,First product: '%i'", j, goal_products[j]);  
        }   
        break;  //The break doesn't really speed up the process since the rest of the threads aren't communicating in this implementation.
                                                                                            
      }
    }  
  }

  double OrderOptimizer::distance(double a[2], double b[2]){                           // Gets distance between 2 points
    return sqrt(pow(b[0] - a[0], 2) + pow(b[1] - a[1], 2) * 1.0);
  }

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<OrderOptimizer>());
  rclcpp::shutdown();
  return 0;
}